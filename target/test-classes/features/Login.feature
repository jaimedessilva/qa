#language: pt
#enconding: UTF-8

@login
Funcionalidade: Login

@loginSucesso
Cenario: Realizar login com sucesso
Quando eu informar o usuario "ADM"
E informar a senha "MASTER"
E acionar o botao login
Entao o sistema apresenta no campo do usuario logado "ADMINISTRADOR"
