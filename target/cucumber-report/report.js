$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ManterCadastro.feature");
formatter.feature({
  "name": "Login",
  "description": "",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "name": "@manterCadastro"
    }
  ]
});
formatter.scenario({
  "name": "editar Usuario",
  "description": "",
  "keyword": "Cenario",
  "tags": [
    {
      "name": "@manterCadastro"
    },
    {
      "name": "@editarUsuario"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "clicar no botao editar",
  "keyword": "Quando "
});
formatter.match({
  "location": "stepsDefinitions.ManterCadastroStep.clicar_no_botao_editar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "informar o campo nome \"PEDRO1\"",
  "keyword": "E "
});
formatter.match({
  "location": "stepsDefinitions.ManterCadastroStep.informar_o_campo_nome(java.lang.String)"
});
formatter.result({
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"#nome\"}\n  (Session info: chrome\u003d84.0.4147.89)\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027DESKTOP-69V3837\u0027, ip: \u0027192.168.56.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_261\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 84.0.4147.89, chrome: {chromedriverVersion: 84.0.4147.30 (48b3e868b4cc0..., userDataDir: C:\\Users\\Jaime\\AppData\\Loca...}, goog:chromeOptions: {debuggerAddress: localhost:49373}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: d04cbe8955fb17969bf6e4f904d1aadf\n*** Element info: {Using\u003did, value\u003dnome}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:372)\r\n\tat org.openqa.selenium.By$ById.findElement(By.java:188)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\r\n\tat com.sun.proxy.$Proxy16.sendKeys(Unknown Source)\r\n\tat pageObjects.ManterUsuario.setNome(ManterUsuario.java:67)\r\n\tat stepsDefinitions.ManterCadastroStep.informar_o_campo_nome(ManterCadastroStep.java:26)\r\n\tat ✽.informar o campo nome \"PEDRO1\"(file:///D:/REPOSITORIOS/stefanini/JAIME_SSILVA/src/test/resources/features/ManterCadastro.feature:23)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "informar o campo matricula \"12345678\"",
  "keyword": "E "
});
formatter.match({
  "location": "stepsDefinitions.ManterCadastroStep.informar_o_campo_matricula(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "clicar no campo Naj",
  "keyword": "E "
});
formatter.match({
  "location": "stepsDefinitions.ManterCadastroStep.clicar_no_campo_Naj()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "clicar no campo cargo",
  "keyword": "E "
});
formatter.match({
  "location": "stepsDefinitions.ManterCadastroStep.clicar_no_campo_cargo()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "informar o campo senha \"@teste123\"",
  "keyword": "E "
});
formatter.match({
  "location": "stepsDefinitions.ManterCadastroStep.informar_o_campo_senha(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "informar no campo tipo de acesso",
  "keyword": "E "
});
formatter.match({
  "location": "stepsDefinitions.ManterCadastroStep.informar_no_campo_tipo_de_acesso()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "clicar em salvar",
  "keyword": "E "
});
formatter.match({
  "location": "stepsDefinitions.ManterCadastroStep.clicar_em_salvar()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});