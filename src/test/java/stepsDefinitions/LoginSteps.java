package stepsDefinitions;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObjects.Login;

/** Project: JAIME_SSILVA
 *  File: LoginSteps.java
 *  @author Jaime Des
 *  Date: 28/07/2020 **/

public class LoginSteps extends Login {
	
	// User
	@Quando("eu informar o usuario {string}")
	public void setUser(String user) {
		sendUserLogin(user);
	}

	// Pass
	@Quando("informar a senha {string}")
	public void setPassword(String pass) {
		sendPassLogin(pass);
	}

	// Click
	@Quando("acionar o botao login")
	public void clickButton() {
		clickBtnLogin();
	}

	// Mensagens
	@Entao("o sistema apresenta a mensagem {string}")
	public void msgLogin(String msg) {
		getMenssage(msg);
		// getUserLogMsg(msg);
	}

	// Usuario Logado
	@Entao("o sistema apresenta no campo do usuario logado {string}")
	public void LogUser(String logado) {
		getUserLogMsg(logado);
	}
}
