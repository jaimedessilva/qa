package runners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/** Project: JAIME_SSILVA
 *  File: Runner.java
 *  @author Jaime Des
 *  Date: 28/07/2020 **/

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"html:target/cucumber-report"},
		features = "src/test/resources/features/",
		glue = "stepsDefinitions",
		dryRun = false,
		tags = {"@editarUsuario"},
		monochrome = true)
public class Runner {}
