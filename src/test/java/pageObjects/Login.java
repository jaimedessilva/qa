package pageObjects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static utils.Utils.driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/** Project: JAIME_SSILVA
 *  File: Login.java
 *  @author Jaime Des
 *  Date: 28/07/2020 **/

public class Login {
	
	/**
	 * Construct
	 */
	public Login() {
		PageFactory.initElements(driver, this);
	}

	// Usuario
	public void sendUserLogin(String user) {
		driver.findElement(By.id("login")).sendKeys(user);
	}

	// Password
	public void sendPassLogin(String pass) {
		driver.findElement(By.id("senha")).sendKeys(pass);

	}

	// Button
	public void clickBtnLogin() {
		driver.findElement(By.xpath("/html/body/div/div/div/form/div[4]/button")).click();
	}

	// Message
	public void getMenssage(String msg) {

		WebElement msgLogin = driver.findElement(By.id("spanMessage"));
		assertEquals(msg, msgLogin.getText());
		System.out.println("Out: " + msgLogin.getText());
	}

	// User Logged
	public void getUserLogMsg (String logado) {

		WebElement txtLogado = driver.findElement(By.xpath("/html/body/nav/ul/li[2]/strong"));
		assertEquals(logado, txtLogado.getText());
		System.out.println("saida: " + txtLogado.getText());
	}
	//Logar
	public void logarSistema() {
		sendUserLogin("ADM");
		sendPassLogin("MASTER");
		clickBtnLogin();

	}
	

}
