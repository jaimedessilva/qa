package pageObjects;

import static utils.Utils.driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/** Project: JAIME_SSILVA
 *  File: ManterUsuario.java
 *  @author Jaime Des
 *  Date: 28/07/2020 **/

public class ManterUsuario {
	
	/* Construct */
	public ManterUsuario() {
		PageFactory.initElements(driver, this);
	}
	/*
	 * Web Element
	 */
	@FindBy(linkText = "Novo Usu�rio")
	WebElement btnNovoUsuario;
	
	@FindBy (xpath = "/html/body/form/div/div/div/div/table/tbody/tr[29]/td[1]/a[2]")
	//@FindBy (linkText = "editar.php?id=83")
	WebElement btnEdit;
	
	@FindBy(xpath = "/html/body/form/div/div/div/div/table/tbody/tr[34]/td[1]/a[3]")
	WebElement btnExcluir;
	
	@FindBy (id = "btsalvar")
	WebElement btncadastrar;
	
	@FindBy(id = "nome")
	WebElement imputNome;
	
	@FindBy(id = "matricula")
	WebElement imputMatricula;
	
	@FindBy(xpath = "//*[@id=\"naj\"]/option[29]")
	WebElement imputNaj;
	
	@FindBy(xpath = "//*[@id=\"cargo\"]/option[23]")
	WebElement imputCargo;
	
	@FindBy(id = "senha")
	WebElement imputSenha;
	
	@FindBy(id = "tipoAcesso")
	WebElement imputTipoAc;
	
	
	// Click Name
	public void clicarUsuario(String nome) {
		driver.findElement(By.linkText(nome)).click();
	}
	//novo
	public void addNew() {
		btnNovoUsuario.click();
	}
	// Nome
	public void setNome (String nome) {
		//imputNome.clear();
		imputNome.sendKeys(nome);
	}
	//Matr
	public void  setMatricula (String mat) {
		//imputMatricula.clear();
		imputMatricula.sendKeys(mat);
	}
	//Naj
	public void setNaj () {
		imputNaj.click();
	}
	//Cargo
	public void setCargo () {
		imputCargo.click();
	}
	//Senha
	public void setSenha (String senha) {
		imputSenha.clear();
		imputSenha.sendKeys(senha);
	}
	//Tipo Ac
	public void setTipoAc () {
		imputTipoAc.click();
	}
	//Save
	public void clickSave () {
		btncadastrar.click();
		System.out.println("passou");
	}
	// Edit
	public void clickEdit() throws InterruptedException {
		Thread.sleep(2000);
		btnEdit.getAttribute("editar.php?id=83");
		btnEdit.click();
		System.out.println("passou");
	}
	public void clickDel () {
		//
	}
	
}
