package utils;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;

/** Project: JAIME_SSILVA
 *  File: UtilsDriver.java
 *  @author Jaime Des
 *  Date: 28/07/2020 **/

public class UtilsDriver {

	// Instancia Driver
	public static WebDriver driver;

	public static void startBrowser() {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.home") + "/driver/chromedriver"); // Linux

		driver = new ChromeDriver();

		// Maximizar Janela
		driver.manage().window().maximize();
		// Espera Implicita de 5 segundos
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Url a navegar
		driver.get("https://app-sgveiculos.herokuapp.com/login");

		// user
		WebElement user = driver.findElement(By.id("username"));
		user.sendKeys("user");

		// user
		WebElement pass = driver.findElement(By.id("password"));
		pass.sendKeys("123");

		// Click logar
		WebElement btnLogin = driver.findElement(By.xpath("/html/body/div/form/div[3]/button"));
		btnLogin.click();

		// Click admin
//		WebElement btnAdmin = driver.findElement(By.xpath("//*[@id=\"wrapper\"]/nav/div[2]/ul/li[1]/a"));
//		btnAdmin.click();
		
		// Click Listas
		WebElement btnLista = driver.findElement(By.linkText("Listas"));
		btnLista.click();
		
		//Click Prop Get Prop By xpath e href
		WebElement btnProp = driver.findElement(By.xpath("//a[@href='/proprietarios']"));
		btnProp.getAttribute("href");
		btnProp.click();
		
		//Edit Id 5 Get Element by xpath e href
		WebElement btnEdit = driver.findElement(By.xpath("//a[@href='/edit-prop/7']"));
		btnEdit.getAttribute("href");
		btnEdit.click();
	    
	}

	@Before
	public static void open() {
		startBrowser();
	}

	@After
	public static void close() throws InterruptedException {
		Thread.sleep(2000);
		driver.quit();
		System.out.println("Passou");
	}

	public static void main(String[] args) throws InterruptedException {
		open();
		close();

	}

}
