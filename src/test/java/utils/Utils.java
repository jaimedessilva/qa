package utils;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.Scenario;

/** Project: JAIME_SSILVA
 *  File: Utils.java
 *  @author Jaime Des
 *  Date: 28/07/2020 **/

public class Utils {
	//Driver
	public static ChromeDriver driver;
	
	//Método
	public static void acessarSistema () {
		//Caminho Driver
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.home")+"/driver/chromedriver.exe");
		
		driver = new ChromeDriver ();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		//Url
		driver.get("https://hackathontestes.herokuapp.com/index.php");
		
	}
	@SuppressWarnings("deprecation")
	public static void capturarScreenshot(Scenario scenario) {
		 final byte[] screen = driver.getScreenshotAs(OutputType.BYTES);
		 scenario.embed(screen, "image/png");
	}
	

}
